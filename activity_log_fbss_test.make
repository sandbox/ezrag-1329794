; The purpose of this make file is to make it faster to setup a fresh Drupal
; environment for testing the Activity log and Facebook-style status modules.
; It is suggested to run this make file with the --working-copy and --make-install flags.

core = 6.x
api = 2

projects[drupal][version] = "6.22"

projects[activity_log][download][type] = "git"
projects[activity_log][download][url] = "http://git.drupal.org/project/activity_log.git"
projects[activity_log][download][revision] = "6.x-2.x"
projects[activity_log][type] = "module"
projects[activity_log][subdir] = "contrib"

projects[digests][download][type] = "git"
projects[digests][download][url] = "http://git.drupal.org/project/digests.git"
projects[digests][download][revision] = "6.x-1.x"
projects[digests][type] = "module"
projects[digests][subdir] = "contrib"

projects[facebook_status][download][type] = "git"
projects[facebook_status][download][url] = "http://git.drupal.org/project/facebook_status.git"
projects[facebook_status][download][revision] = "6.x-3.x"
projects[facebook_status][subdir] = "contrib"

projects[fbsmp][subdir] = "contrib"
projects[fbsmp][download][type] = "git"
projects[fbsmp][download][url] = "http://git.drupal.org/project/fbsmp.git"
projects[fbsmp][download][revision] = 6.x-2.x

projects[ctools][subdir] = "contrib"


projects[views][subdir] = "contrib"


projects[mimemail][subdir] = "contrib"

projects[rules][subdir] = "contrib"

projects[token][subdir] = "contrib"